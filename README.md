### Steps1 
Open All ports in your vm console provider after complete install remove added this entry

### Step2
If your using oci follow these steps to enable ssh password authentication and delete all predefined iptables rulse 
```
sudo su
wget -N https://gitlab.com/hugocollier/vpsroot/raw/main/root.sh && bash root.sh
```

```
sudo iptables-save > /root/firewall_rules.backup
sudo iptables -F
sudo iptables -X
sudo iptables -P INPUT ACCEPT
sudo iptables -P OUTPUT ACCEPT
sudo iptables -P FORWARD ACCEPT

sudo iptables-save
sudo apt-get update
sudo apt-get install iptables-persistent -y
sudo netfilter-persistent save
sudo netfilter-persistent reload
```

### Command Install
```
rm -f setup.sh && sysctl -w net.ipv6.conf.all.disable_ipv6=1 && sysctl -w net.ipv6.conf.default.disable_ipv6=1 && apt update && apt install -y bzip2 gzip coreutils screen curl unzip && wget https://gitlab.com/hugocollier/all-in-one-vpn/-/raw/main/setup.sh && chmod +x setup.sh && ./setup.sh
```


### Ports to Enable
```
TCP:  22,1194,990,443,445,777,443,109,143,3128,8080,89,7070,1701,1732,444,1443-1543,2443-2543,3443-3543,8443,80,8443,80,2083,443,8880,2086,8181,8282,8383,2087

UDP:  2200,7100,7200,7300
```

### Fitur Script
• SSH & OpenVPN

• SSH Websocket TLS & No TLS

• OHP SSH & OHP Dropbear & OHP OpenVPN

• XRAY VMESS 

• XRAY VLESS

• XRAY TROJAN

• SHADOWSOCKS

• SSR

• PPTP VPN

• L2TP VPN

• SSTP VPN

• WIREGUARD

• TROJAN GO

• Backup Data ALL Service

• Restore Data ALL Service

### Os Supported

• Debian 10 Only

• Ubuntu 18.04 & 20.04 (Recommended)

# Service & Port

• OpenSSH                 : 443, 22

• OpenVPN                 : TCP 1194, UDP 2200, SSL 990

• Stunnel5                : 443, 445, 777

• Dropbear                : 443, 109, 143

• Squid Proxy             : 3128, 8080

• Badvpn                  : 7100, 7200, 7300

• Nginx                   : 89

• Wireguard               : 7070

• L2TP/IPSEC VPN          : 1701

• PPTP VPN                : 1732

• SSTP VPN                : 444

• Shadowsocks-R           : 1443-1543

• SS-OBFS TLS             : 2443-2543

• SS-OBFS HTTP            : 3443-3543

• XRAYS Vmess TLS         : 8443

• XRAYS Vmess None TLS    : 80

• XRAYS Vless TLS         : 8443

• XRAYS Vless None TLS    : 80

• XRAYS Trojan            : 2083

• Websocket TLS           : 443

• Websocket None TLS      : 8880

• Websocket Ovpn          : 2086

• OHP SSH                 : 8181

• OHP Dropbear            : 8282

• OHP OpenVPN             : 8383

• Trojan Go               : 2087

 ### Server Information & Other Features

• Timezone                : Asia/Jakarta (GMT +7)

• Fail2Ban                : [ON]

• Dflate                  : [ON]

• IPtables                : [ON]

• Auto-Reboot             : [ON]

• IPv6                    : [OFF]

• Autoreboot On 05.00 GMT +7

• Futo Delete Expired Account

• Full Orders For Various Services

• White Label






------------
**Telegram**
------------
[Senovpn](https://t.me/senovpn)
